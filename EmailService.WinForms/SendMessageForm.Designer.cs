﻿namespace EmailService.WinForms
{
    partial class SendMessageForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSend = new System.Windows.Forms.Button();
            this.timePickerSendAt = new System.Windows.Forms.DateTimePicker();
            this.lblTo = new System.Windows.Forms.Label();
            this.lblSubject = new System.Windows.Forms.Label();
            this.lblMessage = new System.Windows.Forms.Label();
            this.txtBoxTo = new System.Windows.Forms.TextBox();
            this.txtBoxSubject = new System.Windows.Forms.TextBox();
            this.rtBoxMessage = new System.Windows.Forms.RichTextBox();
            this.lblSend = new System.Windows.Forms.Label();
            this.lblNow = new System.Windows.Forms.Label();
            this.lblAt = new System.Windows.Forms.Label();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.lblValidationMessage = new System.Windows.Forms.Label();
            this.btnLoad = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblMessagesLoaded = new System.Windows.Forms.Label();
            this.btnSendMessages = new System.Windows.Forms.Button();
            this.lblMessagesToSend = new System.Windows.Forms.Label();
            this.btnViewReport = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnSend
            // 
            this.btnSend.Location = new System.Drawing.Point(259, 226);
            this.btnSend.Name = "btnSend";
            this.btnSend.Size = new System.Drawing.Size(89, 32);
            this.btnSend.TabIndex = 0;
            this.btnSend.Text = "Send";
            this.btnSend.UseVisualStyleBackColor = true;
            this.btnSend.Click += new System.EventHandler(this.btnSend_Click);
            // 
            // timePickerSendAt
            // 
            this.timePickerSendAt.CustomFormat = "dd.MM.yyyy HH:mm";
            this.timePickerSendAt.Enabled = false;
            this.timePickerSendAt.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.timePickerSendAt.Location = new System.Drawing.Point(94, 230);
            this.timePickerSendAt.Name = "timePickerSendAt";
            this.timePickerSendAt.ShowUpDown = true;
            this.timePickerSendAt.Size = new System.Drawing.Size(114, 20);
            this.timePickerSendAt.TabIndex = 1;
            this.timePickerSendAt.Value = new System.DateTime(2016, 6, 28, 11, 28, 1, 0);
            // 
            // lblTo
            // 
            this.lblTo.AutoSize = true;
            this.lblTo.Location = new System.Drawing.Point(13, 10);
            this.lblTo.Name = "lblTo";
            this.lblTo.Size = new System.Drawing.Size(20, 13);
            this.lblTo.TabIndex = 2;
            this.lblTo.Text = "To";
            // 
            // lblSubject
            // 
            this.lblSubject.AutoSize = true;
            this.lblSubject.Location = new System.Drawing.Point(12, 36);
            this.lblSubject.Name = "lblSubject";
            this.lblSubject.Size = new System.Drawing.Size(43, 13);
            this.lblSubject.TabIndex = 3;
            this.lblSubject.Text = "Subject";
            // 
            // lblMessage
            // 
            this.lblMessage.AutoSize = true;
            this.lblMessage.Location = new System.Drawing.Point(12, 65);
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(50, 13);
            this.lblMessage.TabIndex = 4;
            this.lblMessage.Text = "Message";
            // 
            // txtBoxTo
            // 
            this.txtBoxTo.Location = new System.Drawing.Point(68, 7);
            this.txtBoxTo.Name = "txtBoxTo";
            this.txtBoxTo.Size = new System.Drawing.Size(200, 20);
            this.txtBoxTo.TabIndex = 5;
            // 
            // txtBoxSubject
            // 
            this.txtBoxSubject.Location = new System.Drawing.Point(68, 36);
            this.txtBoxSubject.Name = "txtBoxSubject";
            this.txtBoxSubject.Size = new System.Drawing.Size(200, 20);
            this.txtBoxSubject.TabIndex = 6;
            // 
            // rtBoxMessage
            // 
            this.rtBoxMessage.Location = new System.Drawing.Point(68, 62);
            this.rtBoxMessage.Name = "rtBoxMessage";
            this.rtBoxMessage.Size = new System.Drawing.Size(284, 136);
            this.rtBoxMessage.TabIndex = 7;
            this.rtBoxMessage.Text = "";
            // 
            // lblSend
            // 
            this.lblSend.AutoSize = true;
            this.lblSend.Location = new System.Drawing.Point(15, 207);
            this.lblSend.Name = "lblSend";
            this.lblSend.Size = new System.Drawing.Size(35, 13);
            this.lblSend.TabIndex = 8;
            this.lblSend.Text = "Send:";
            // 
            // lblNow
            // 
            this.lblNow.AutoSize = true;
            this.lblNow.Location = new System.Drawing.Point(65, 207);
            this.lblNow.Name = "lblNow";
            this.lblNow.Size = new System.Drawing.Size(29, 13);
            this.lblNow.TabIndex = 9;
            this.lblNow.Text = "Now";
            // 
            // lblAt
            // 
            this.lblAt.AutoSize = true;
            this.lblAt.Location = new System.Drawing.Point(68, 232);
            this.lblAt.Name = "lblAt";
            this.lblAt.Size = new System.Drawing.Size(20, 13);
            this.lblAt.TabIndex = 10;
            this.lblAt.Text = "At:";
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Checked = true;
            this.checkBox1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox1.Location = new System.Drawing.Point(100, 207);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(15, 14);
            this.checkBox1.TabIndex = 11;
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // lblValidationMessage
            // 
            this.lblValidationMessage.AutoSize = true;
            this.lblValidationMessage.ForeColor = System.Drawing.Color.DarkRed;
            this.lblValidationMessage.Location = new System.Drawing.Point(208, 207);
            this.lblValidationMessage.Name = "lblValidationMessage";
            this.lblValidationMessage.Size = new System.Drawing.Size(0, 13);
            this.lblValidationMessage.TabIndex = 12;
            // 
            // btnLoad
            // 
            this.btnLoad.Location = new System.Drawing.Point(6, 19);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(114, 32);
            this.btnLoad.TabIndex = 13;
            this.btnLoad.Text = "Load from file";
            this.btnLoad.UseVisualStyleBackColor = true;
            this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lblMessagesLoaded);
            this.groupBox1.Controls.Add(this.btnSendMessages);
            this.groupBox1.Controls.Add(this.lblMessagesToSend);
            this.groupBox1.Controls.Add(this.btnLoad);
            this.groupBox1.Location = new System.Drawing.Point(18, 273);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(330, 100);
            this.groupBox1.TabIndex = 14;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "File";
            // 
            // lblMessagesLoaded
            // 
            this.lblMessagesLoaded.AutoSize = true;
            this.lblMessagesLoaded.Location = new System.Drawing.Point(237, 69);
            this.lblMessagesLoaded.Name = "lblMessagesLoaded";
            this.lblMessagesLoaded.Size = new System.Drawing.Size(13, 13);
            this.lblMessagesLoaded.TabIndex = 16;
            this.lblMessagesLoaded.Text = "0";
            // 
            // btnSendMessages
            // 
            this.btnSendMessages.Location = new System.Drawing.Point(6, 59);
            this.btnSendMessages.Name = "btnSendMessages";
            this.btnSendMessages.Size = new System.Drawing.Size(114, 32);
            this.btnSendMessages.TabIndex = 15;
            this.btnSendMessages.Text = "Send Messages";
            this.btnSendMessages.UseVisualStyleBackColor = true;
            this.btnSendMessages.Click += new System.EventHandler(this.btnSendMessages_Click);
            // 
            // lblMessagesToSend
            // 
            this.lblMessagesToSend.AutoSize = true;
            this.lblMessagesToSend.Location = new System.Drawing.Point(135, 69);
            this.lblMessagesToSend.Name = "lblMessagesToSend";
            this.lblMessagesToSend.Size = new System.Drawing.Size(99, 13);
            this.lblMessagesToSend.TabIndex = 14;
            this.lblMessagesToSend.Text = "Messages to send: ";
            // 
            // btnViewReport
            // 
            this.btnViewReport.Location = new System.Drawing.Point(18, 390);
            this.btnViewReport.Name = "btnViewReport";
            this.btnViewReport.Size = new System.Drawing.Size(120, 23);
            this.btnViewReport.TabIndex = 15;
            this.btnViewReport.Text = "View Report";
            this.btnViewReport.UseVisualStyleBackColor = true;
            this.btnViewReport.Click += new System.EventHandler(this.btnViewReport_Click);
            // 
            // SendMessageForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(363, 425);
            this.Controls.Add(this.btnViewReport);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.lblValidationMessage);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.lblAt);
            this.Controls.Add(this.lblNow);
            this.Controls.Add(this.lblSend);
            this.Controls.Add(this.rtBoxMessage);
            this.Controls.Add(this.txtBoxSubject);
            this.Controls.Add(this.txtBoxTo);
            this.Controls.Add(this.lblMessage);
            this.Controls.Add(this.lblSubject);
            this.Controls.Add(this.lblTo);
            this.Controls.Add(this.timePickerSendAt);
            this.Controls.Add(this.btnSend);
            this.Name = "SendMessageForm";
            this.Text = "SendMessage";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnSend;
        private System.Windows.Forms.DateTimePicker timePickerSendAt;
        private System.Windows.Forms.Label lblTo;
        private System.Windows.Forms.Label lblSubject;
        private System.Windows.Forms.Label lblMessage;
        private System.Windows.Forms.TextBox txtBoxTo;
        private System.Windows.Forms.TextBox txtBoxSubject;
        private System.Windows.Forms.RichTextBox rtBoxMessage;
        private System.Windows.Forms.Label lblSend;
        private System.Windows.Forms.Label lblNow;
        private System.Windows.Forms.Label lblAt;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Label lblValidationMessage;
        private System.Windows.Forms.Button btnLoad;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnSendMessages;
        private System.Windows.Forms.Label lblMessagesToSend;
        private System.Windows.Forms.Label lblMessagesLoaded;
        private System.Windows.Forms.Button btnViewReport;
    }
}

