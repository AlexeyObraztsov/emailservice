﻿using System;
using System.IO;
using EmailService.Web.Core.MailService;
using EmailService.Web.Core.Presentors;
using NUnit.Framework;
using Moq;
using EmailService.Web.Core.Views;

namespace EmailService.UnitTests.Presentors
{
    [TestFixture]
    public class SendMailPresentorTests
    {
        private string _solutionRoot = Directory.GetParent(AppDomain.CurrentDomain.BaseDirectory).Parent.Parent.FullName;

        [Test]
        public void OnSendMessage_EventWasRisen()
        {
            //Arrange
            var viewMock = GetViewMock();
            var view = viewMock.Object;
            var presentor = new SendMailPresentor(view, GetMailServiceMock().Object);
            var wasCalled = false;
            view.SendMessage += (o, e) => wasCalled = true;

            //Act
            viewMock.Raise(m=> m.SendMessage += null , EventArgs.Empty);

            //Assert
            Assert.IsTrue(wasCalled);
        }

        [Test]
        public void OnSendMessages_EventWasRisen()
        {
            //Arrange
            var viewMock = GetViewMock();
            viewMock.Setup(m => m.UploadedFileStream)
                .Returns(new FileStream($@"{_solutionRoot}\mails.csv", FileMode.Open, FileAccess.Read));
            var view = viewMock.Object;
            var presentor = new SendMailPresentor(view, GetMailServiceMock().Object);
            var wasCalled = false;
            view.SendMessages += (o, e) => wasCalled = true;

            //Act
            viewMock.Raise(m => m.SendMessages += null, EventArgs.Empty);

            //Assert
            Assert.IsTrue(wasCalled);

            
        }

        [Test]
        public void UploadCsv_SuccsessfulUploadAttempt()
        {
            //Arrange
            var viewMock = GetViewMock();
            viewMock.Setup(m => m.UploadedFileStream)
               .Returns(new FileStream($@"{_solutionRoot}\mails.csv", FileMode.Open, FileAccess.Read));
            var view = viewMock.Object;
            var presentor = new SendMailPresentor(view, GetMailServiceMock().Object);

            //Act
            presentor.UploadCsv();

            //Assert
            Assert.IsNotNull(presentor.UploadInfo);
        }

        [Test]
        public void UploadCsv_MailUploaded()
        {
            //Arrange
            var viewMock = GetViewMock();
            viewMock.Setup(m => m.UploadedFileStream)
               .Returns(new FileStream($@"{_solutionRoot}\mails.csv", FileMode.Open, FileAccess.Read));
            var view = viewMock.Object;
            var presentor = new SendMailPresentor(view, GetMailServiceMock().Object);

            //Act
            presentor.UploadCsv();

            //Assert
            Assert.IsTrue(presentor.UploadInfo.UploadedMessagesCount > 0);
        }

        [Test]
        public void SendMessage_ServiceWasCalled()
        {
            //Arrange
            var serviceMock = GetMailServiceMock();
            var service = serviceMock.Object;
            var presentor = new SendMailPresentor(GetViewMock().Object, service);

            //Act & Assert
            presentor.SendMessage(It.IsAny<EmailMessage>());
            serviceMock.Verify(m => m.SendMessage(It.IsAny<EmailMessage>()));
        }

        [Test]
        public void SendMessages_ServiceWasCalled()
        {
            //Arrange
            var serviceMock = GetMailServiceMock();
            var service = serviceMock.Object;
            var presentor = new SendMailPresentor(GetViewMock().Object, service);

            //Act & Assert
            presentor.SendMessages(It.IsAny<EmailMessage[]>());
            serviceMock.Verify(m => m.SendMessages(It.IsAny<EmailMessage[]>()));
        }

        [Test]
        public void CreateMessage_FromCorrectParts_Succsessful()
        {
            //Arrange
            var parts = new[]
            {
                @"hello@gmail.com",
                @"hello",
                @"¯\_(ツ)_/¯",
                @"15:29"
            };
            var presentor = new SendMailPresentor(GetViewMock().Object, GetMailServiceMock().Object);

            //Act
            var message = presentor.CreateMessage(parts);

            //Assert
            Assert.IsNotNull(message);
        }

        [Test]
        public void CreateMessage_WithEmptyParts_ReturnsNull()
        {
            //Arrange
            var parts = new[]
            {
                @"hello@gmail.com",
                @"hello",
                string.Empty,
                string.Empty
            };
            var presentor = new SendMailPresentor(GetViewMock().Object, GetMailServiceMock().Object);

            //Act
            var message = presentor.CreateMessage(parts);

            //Assert
            Assert.IsNull(message);
        }

        [Test]
        public void CreateMessage_WithNullParts_ReturnsNull()
        {
            //Arrange
            var parts = new[]
            {
                @"hello@gmail.com",
                @"hello",
                null,
                null
            };
            var presentor = new SendMailPresentor(GetViewMock().Object, GetMailServiceMock().Object);

            //Act
            var message = presentor.CreateMessage(parts);

            //Assert
            Assert.IsNull(message);
        }

        private Mock<ISendMailView> GetViewMock()
        {
            var mock = new Mock<ISendMailView>();
            mock.Setup(m => m.From).Returns("noreply@host.org");
            mock.Setup(m => m.To).Returns("hello@gmial.com");
            mock.Setup(m => m.Subject).Returns("hi");
            mock.Setup(m => m.Message).Returns("message");
            mock.Setup(m => m.SendDate).Returns(DateTime.Parse("2016-08-19 11:00"));

            return mock;
        }

        private Mock<IMailService> GetMailServiceMock()
        {
            var mock = new Mock<IMailService>();
            mock.Setup(m => m.SendMessage(It.IsAny<EmailMessage>())).Verifiable();
            mock.Setup(m => m.SendMessages(It.IsAny<EmailMessage[]>())).Verifiable();
            return mock;
        }
    }
}
