﻿USE EmailServiceDB
GO
-----
--AddUser---
IF EXISTS ( SELECT * 
            FROM   sysobjects 
            WHERE  id = object_id(N'[dbo].[AddUser]') 
                   and OBJECTPROPERTY(id, N'IsProcedure') = 1 )
    DROP PROCEDURE [dbo].[AddUser]
GO

CREATE PROCEDURE AddUser
	@userAdress nvarchar(50),
	@userID uniqueidentifier OUTPUT
AS
BEGIN

	IF NOT EXISTS(SELECT Adress FROM MailUsers WHERE Adress = @userAdress)
		BEGIN
		SET @userId = NEWID()
			INSERT INTO MailUsers (Id, Adress) VALUES(@userID, @userAdress)
		END
	ELSE
		BEGIN
			SET @userID = (SELECT TOP 1 Id FROM MailUsers WHERE Adress = @userAdress)
		END
END
GO
-----

--AddMessage---
IF EXISTS ( SELECT * 
            FROM   sysobjects 
            WHERE  id = object_id(N'[dbo].[AddMessage]') 
                   and OBJECTPROPERTY(id, N'IsProcedure') = 1 )
    DROP PROCEDURE [dbo].[AddMessage]
GO

CREATE PROCEDURE AddMessage
	@messageId uniqueidentifier,
	@statusId int,
	@text nvarchar(MAX),
	@subject nvarchar(50),
	@sendDate datetime,
	@fromUser nvarchar(50),
	@toUser nvarchar(50)
AS
BEGIN
	DECLARE @fromUserId uniqueidentifier
	DECLARE @toUserId uniqueidentifier

	EXEC AddUser @fromUser, @userID = @fromUserId OUTPUT
	EXEC AddUser @toUser, @userID = @toUserId OUTPUT

	INSERT INTO Messages (Id, StatusId, Text, Subject, SendDate, FromUser) VALUES (@messageId, @statusId, @text, @subject, @sendDate, @fromUserId)
	INSERT INTO MailUsersMessages (MessageId, MailUserId) VALUES (@messageId, @toUserId)
	
END
GO
-----

--UpdateMessageStatus---
IF EXISTS ( SELECT * 
            FROM   sysobjects 
            WHERE  id = object_id(N'[dbo].[UpdateMessageStatus]') 
                   and OBJECTPROPERTY(id, N'IsProcedure') = 1 )
    DROP PROCEDURE [dbo].[UpdateMessageStatus]
GO

CREATE PROCEDURE UpdateMessageStatus
	@id uniqueidentifier,
	@status int
AS
BEGIN
	UPDATE Messages SET StatusId = @status WHERE Id = @id
END
GO
-----

--AddRecipient---
IF EXISTS ( SELECT * 
            FROM   sysobjects 
            WHERE  id = object_id(N'[dbo].[AddRecipient]') 
                   and OBJECTPROPERTY(id, N'IsProcedure') = 1 )
    DROP PROCEDURE [dbo].[AddRecipient]
GO

CREATE PROCEDURE AddRecipient
	@messageId uniqueidentifier,
	@adress nvarchar(50)
AS
BEGIN
	EXEC AddUser @adress

	DECLARE @recipient uniqueidentifier 
	SET @recipient = (SELECT TOP 1 Id FROM MailUsers WHERE Adress = @adress)
	INSERT INTO MailUsersMessages (MessageId, MailUserId) VALUES (@messageId, @recipient)
END
GO
-----

--GetRecipients---
IF EXISTS ( SELECT * 
            FROM   sysobjects 
            WHERE  id = object_id(N'[dbo].[GetRecipients]') 
                   and OBJECTPROPERTY(id, N'IsProcedure') = 1 )
    DROP PROCEDURE [dbo].[GetRecipients]
GO

CREATE PROCEDURE GetRecipients
	@messageId uniqueidentifier
AS
BEGIN
	SELECT u.Adress FROM MailUsers AS u INNER JOIN MailUsersMessages AS um ON u.Id = um.MailUserId
					WHERE um.MessageId = @messageId
END
GO


--GetMessagesToSend---
IF EXISTS ( SELECT * 
            FROM   sysobjects 
            WHERE  id = object_id(N'[dbo].[GetMessagesToSend]') 
                   and OBJECTPROPERTY(id, N'IsProcedure') = 1 )
    DROP PROCEDURE [dbo].[GetMessagesToSend]
GO

CREATE PROCEDURE GetMessagesToSend
AS
BEGIN
	SELECT m.Id, m.Text, m.Subject, m.SendDate, u.Adress AS 'FromUser', m.StatusId AS 'Status', mu.Adress AS 'ToUser'
	FROM Messages m
	JOIN MailUsers u ON m.FromUser = u.Id
	JOIN MailUsersMessages um ON m.Id = um.MessageId
	JOIN MailUsers mu ON mu.Id = um.MailUserId
	WHERE m.SendDate <= GETDATE()
	AND m.StatusId IN (1, 4)
	ORDER BY m.StatusId
END
GO

--GetMessagesToReport---
IF EXISTS ( SELECT * 
            FROM   sysobjects 
            WHERE  id = object_id(N'[dbo].[GetMessagesToReport]') 
                   and OBJECTPROPERTY(id, N'IsProcedure') = 1 )
    DROP PROCEDURE [dbo].[GetMessagesToReport]
GO

CREATE PROCEDURE GetMessagesToReport
	@fromUser nvarchar(50),
	@startDate datetime,
	@endDate datetime
AS
BEGIN
	SELECT m.StatusId, m.Subject, m.SendDate
	FROM Messages m
	JOIN MailUsers u ON u.Adress = @fromUser 
	WHERE m.SendDate BETWEEN @startDate AND @endDate
END
GO