﻿namespace EmailService.Web.Core.Presentors
{
    public interface IPresentor
    {
        void InitView(bool isPostBack);
    }
}