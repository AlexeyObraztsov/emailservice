﻿namespace EmailService.Domain.Enums
{
    public enum MailStatus
    {
        Pending = 1,
        Sended = 2,
        Undelivered = 3,
        MailboxUnavailible = 4
    }
}