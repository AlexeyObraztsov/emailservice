﻿namespace EmailService.WinForms
{
    partial class MailReportsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            this.mailReportViewer = new Microsoft.Reporting.WinForms.ReportViewer();
            this.lblStartDate = new System.Windows.Forms.Label();
            this.lblEndDate = new System.Windows.Forms.Label();
            this.timePickerStartDate = new System.Windows.Forms.DateTimePicker();
            this.timePickerEndDate = new System.Windows.Forms.DateTimePicker();
            this.btnView = new System.Windows.Forms.Button();
            this.MailReportBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.MailReportBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // mailReportViewer
            // 
            this.mailReportViewer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            reportDataSource1.Name = "ReportDataSet";
            reportDataSource1.Value = this.MailReportBindingSource;
            this.mailReportViewer.LocalReport.DataSources.Add(reportDataSource1);
            this.mailReportViewer.LocalReport.ReportEmbeddedResource = "EmailService.WinForms.Report.rdlc";
            this.mailReportViewer.Location = new System.Drawing.Point(12, 62);
            this.mailReportViewer.Name = "mailReportViewer";
            this.mailReportViewer.Size = new System.Drawing.Size(495, 340);
            this.mailReportViewer.TabIndex = 0;
            // 
            // lblStartDate
            // 
            this.lblStartDate.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblStartDate.AutoSize = true;
            this.lblStartDate.Location = new System.Drawing.Point(-2, 21);
            this.lblStartDate.Name = "lblStartDate";
            this.lblStartDate.Size = new System.Drawing.Size(53, 13);
            this.lblStartDate.TabIndex = 1;
            this.lblStartDate.Text = "Start date";
            // 
            // lblEndDate
            // 
            this.lblEndDate.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblEndDate.AutoSize = true;
            this.lblEndDate.Location = new System.Drawing.Point(205, 22);
            this.lblEndDate.Name = "lblEndDate";
            this.lblEndDate.Size = new System.Drawing.Size(50, 13);
            this.lblEndDate.TabIndex = 2;
            this.lblEndDate.Text = "End date";
            // 
            // timePickerStartDate
            // 
            this.timePickerStartDate.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.timePickerStartDate.CustomFormat = "dd.MM.yyyy HH:mm";
            this.timePickerStartDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.timePickerStartDate.Location = new System.Drawing.Point(57, 19);
            this.timePickerStartDate.Name = "timePickerStartDate";
            this.timePickerStartDate.Size = new System.Drawing.Size(128, 20);
            this.timePickerStartDate.TabIndex = 3;
            this.timePickerStartDate.Value = new System.DateTime(2016, 1, 1, 0, 0, 0, 0);
            // 
            // timePickerEndDate
            // 
            this.timePickerEndDate.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.timePickerEndDate.CustomFormat = "dd.MM.yyyy HH:mm";
            this.timePickerEndDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.timePickerEndDate.Location = new System.Drawing.Point(261, 19);
            this.timePickerEndDate.Name = "timePickerEndDate";
            this.timePickerEndDate.Size = new System.Drawing.Size(124, 20);
            this.timePickerEndDate.TabIndex = 4;
            // 
            // btnView
            // 
            this.btnView.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnView.Location = new System.Drawing.Point(408, 11);
            this.btnView.Name = "btnView";
            this.btnView.Size = new System.Drawing.Size(99, 40);
            this.btnView.TabIndex = 5;
            this.btnView.Text = "View";
            this.btnView.UseVisualStyleBackColor = true;
            this.btnView.Click += new System.EventHandler(this.btnView_Click);
            // 
            // MailReportBindingSource
            // 
            this.MailReportBindingSource.DataSource = typeof(EmailService.WinForms.ReportService.MailReport);
            // 
            // MailReportsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(519, 414);
            this.Controls.Add(this.btnView);
            this.Controls.Add(this.timePickerEndDate);
            this.Controls.Add(this.timePickerStartDate);
            this.Controls.Add(this.lblEndDate);
            this.Controls.Add(this.lblStartDate);
            this.Controls.Add(this.mailReportViewer);
            this.Name = "MailReportsForm";
            this.Text = "Mail Reports";
            this.Load += new System.EventHandler(this.MailReportsForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.MailReportBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer mailReportViewer;
        private System.Windows.Forms.BindingSource MailReportBindingSource;
        private System.Windows.Forms.Label lblStartDate;
        private System.Windows.Forms.Label lblEndDate;
        private System.Windows.Forms.DateTimePicker timePickerStartDate;
        private System.Windows.Forms.DateTimePicker timePickerEndDate;
        private System.Windows.Forms.Button btnView;
    }
}