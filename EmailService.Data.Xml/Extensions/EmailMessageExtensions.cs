﻿using System.Collections.Generic;
using System.Linq;
using EmailService.Domain.Entity;

namespace EmailService.Data.Xml.Extensions
{
    public static class EmailMessageExtensions
    {
        public static EmailMessage ToDomain(this EmailMessageDTO dto)
        {
            var message = new EmailMessage
            {
                Id = dto.Id,
                From = dto.From,
                To = dto.To,
                Subject = dto.Subject,
                Message = dto.Message,
                Status = dto.Status,
                SendDate = dto.SendDate
            };
            return message;
        }

        public static EmailMessageDTO ToDto(this EmailMessage message)
        {
            var dto = new EmailMessageDTO
            {
                Id = message.Id,
                From = message.From,
                To = message.To,
                Subject = message.Subject,
                Message = message.Message,
                Status = message.Status,
                SendDate = message.SendDate
            };
            return dto;
        }

        public static List<EmailMessage> ToDomainCollection(this IEnumerable<EmailMessageDTO> dtos)
        {
            return dtos.Select(dto => dto.ToDomain()).ToList();
        }

        public static List<EmailMessageDTO> ToDtoCollection(this IEnumerable<EmailMessage> messages)
        {
            return messages.Select(message => message.ToDto()).ToList();
        }
    }
}
