﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EmailService.Infrastructure.Dependency;
using EmailService.Infrastructure.Logger;
using EmailService.Infrastructure.Unity;
using EmailService.Web.Core.Presentors;
using EmailService.Web.Core.Views;


namespace EmailService.WebForms
{
    public partial class _Default : Page, ISendMailView
    {
        private ILogger _log;
        private IServiceLocator _serviceLocator;
        private readonly SendMailPresentor _presentor;

        private static string _clientAdress = "noreply@host.org";

        public _Default()
        {
            _presentor = new SendMailPresentor(this);
            _serviceLocator = ServiceLocator.GetInstance();
            _log = _serviceLocator.GetService<ILogger>();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            _presentor.InitView(IsPostBack);
            
            _log.Debug("Page Load");
        }

        #region Events Forwarding

        protected void BtnSend_OnServerClick(object sender, EventArgs e)
        {
            if (SendMessage == null) return;
            SendMessage(this, EventArgs.Empty);
            LblSuccess.InnerText = "Success!";
        }

        protected void BtnSendMessages_OnServerClick(object sender, EventArgs e)
        {
            var extension = Path.GetExtension(UploadedFile.PostedFile.FileName);
            if (extension != ".csv")
            {
                LblLoadError.InnerText = @"Error: Please, upload '.csv' file";
                return;
            }

            if (SendMessages == null) return;
            SendMessages(this, EventArgs.Empty);
            LblLoaded.InnerText = _presentor.UploadInfo.UploadedMessagesCount.ToString();
            LblLoadError.InnerText = $"{_presentor.UploadInfo.ErrorLinesInfo} {_presentor.UploadInfo.InvalidLinesInfo}";
            if (_presentor.UploadInfo.UploadedMessagesCount == 0)
            {
                _log.Debug("No messages loaded");
            }
        }

        #endregion

        #region ISendMailView members

        public string To
        {
            get { return TextBoxTo.Value; }
            set { TextBoxTo.Value = value; }
        }

        public string From
        {
            get { return _clientAdress; }
            set { _clientAdress = value; }
        }
        public string Subject
        {
            get { return TextBoxSubject.Value; }
            set { TextBoxSubject.Value = value; }
        }

        public string Message
        {
            get { return TextBoxMessage.Value; }
            set { TextBoxMessage.Value = value; }
        }

        public DateTime SendDate
        {
            get
            {
                if (CheckboxNow.Checked) return DateTime.Now;
                var sendDateString = TimePickerSendDate.Value.Replace('T', ' ');
                return DateTime.Parse(sendDateString);
            }
            set { TimePickerSendDate.Value = value.ToString("yyyy-MM-ddTHH:mm"); }
        }

        public Stream UploadedFileStream
        {
            get { return UploadedFile.PostedFile.InputStream; }
        }

        public event EventHandler SendMessage;
        public event EventHandler SendMessages;

        public void ClearForm()
        {
            To = string.Empty;
            Subject = string.Empty;
            Message = string.Empty;
            SendDate = DateTime.Now;
            CheckboxNow.Checked = true;
        }

        public void ClearInfo()
        {
            LblSuccess.InnerText = string.Empty;
            LblLoaded.InnerText = string.Empty;
            LblLoadError.InnerText = string.Empty;
        }

        #endregion
    }
}