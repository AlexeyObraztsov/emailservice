﻿using System;
using System.Windows.Forms;
using EmailService.WinForms.ReportService;
using Microsoft.Reporting.WinForms;

namespace EmailService.WinForms
{
    public partial class MailReportsForm : Form
    {
        private const string ClientAdress = "noreply@host.org";

        public MailReportsForm()
        {
            InitializeComponent();
        }

        private void MailReportsForm_Load(object sender, EventArgs e)
        {
            ViewReport();
        }

        private void ViewReport()
        {
            
            var client = new ReportServiceClient();
            var reports = client.GetMailReports(ClientAdress, timePickerStartDate.Value, timePickerEndDate.Value);
            client.Close();
            var reportDataSource = new ReportDataSource("ReportDataSet", reports);

            mailReportViewer.LocalReport.DataSources.Clear();
            mailReportViewer.LocalReport.DataSources.Add(reportDataSource);

            var reportParams = new[]
            {
                new ReportParameter("start", timePickerStartDate.Text),
                new ReportParameter("end", timePickerEndDate.Text),
                new ReportParameter("user", ClientAdress)
            };

            mailReportViewer.LocalReport.SetParameters(reportParams);
            mailReportViewer.LocalReport.Refresh();
        }

        private void btnView_Click(object sender, EventArgs e)
        {
            ViewReport();
            mailReportViewer.RefreshReport();
        }
    }
}
