﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/
USE EmailServiceDB
GO
-- MailUsers---
IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'MailUsers' AND TABLE_SCHEMA = 'dbo')
    DROP TABLE dbo.MailUsers;
GO

CREATE TABLE MailUsers(
	Id uniqueidentifier NOT NULL,
	Adress nvarchar(50) NOT NULL
	)
GO

ALTER TABLE MailUsers ADD CONSTRAINT PK_MalUsers PRIMARY KEY(Id)
GO

ALTER TABLE MailUsers ADD CONSTRAINT UQ_MailUserAdress UNIQUE(Adress)
GO
-----------------------------------------

--MailStatus----
IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'MailStatus' AND TABLE_SCHEMA = 'dbo')
    DROP TABLE dbo.MailStatus;
GO

CREATE TABLE MailStatus(
	Id int NOT NULL,
	Status nvarchar(50) NOT NULL
	)
GO

ALTER TABLE MailStatus ADD CONSTRAINT PK_MailStatus PRIMARY KEY(Id)
GO

ALTER TABLE MailStatus ADD CONSTRAINT UQ_MailStatus UNIQUE(Status)
GO
-----------------------------------------

--Messages---
IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Messages' AND TABLE_SCHEMA = 'dbo')
    DROP TABLE dbo.Messages;
GO

CREATE TABLE Messages(
	Id uniqueidentifier NOT NULL,
	StatusId int NOT NULL,
	Text nvarchar(MAX),
	Subject nvarchar(50) NOT NULL,
	SendDate datetime NOT NULL,
	FromUser uniqueidentifier NOT NULL
	)
GO

ALTER TABLE Messages ADD CONSTRAINT PK_Messages PRIMARY KEY(Id)
GO

ALTER TABLE Messages ADD CONSTRAINT FK_MailUsers FOREIGN KEY(FromUser)
REFERENCES MailUsers (Id)
GO

ALTER TABLE Messages ADD CONSTRAINT FK_MailStatus FOREIGN KEY(StatusId)
REFERENCES MailStatus (Id)
GO

CREATE INDEX IX_FromUser
ON Messages (FromUser)

CREATE INDEX IX_SendDate
ON Messages (SendDate)
--------------------------------------

--MailUsersMessages---
IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'MailUsersMessages' AND TABLE_SCHEMA = 'dbo')
    DROP TABLE dbo.MailUsersMessages;
GO

CREATE TABLE MailUsersMessages(
	MessageId uniqueidentifier NOT NULL,
	MailUserId uniqueidentifier NOT NULL
)
GO

ALTER TABLE MailUsersMessages ADD CONSTRAINT PK_MailUsersMessages PRIMARY KEY(MessageId, MailUserId)
GO

ALTER TABLE MailUsersMessages ADD CONSTRAINT FK_MailUsersMessages_Messages FOREIGN KEY(MessageId)
REFERENCES Messages (Id)
GO

ALTER TABLE MailUsersMessages ADD CONSTRAINT FK_MailUsersMessages_Users FOREIGN KEY(MailUserId)
REFERENCES MailUsers (Id)
GO
