﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using EmailService.Domain.Entity;

namespace EmailService.WCF.Report
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IReportService" in both code and config file together.
    [ServiceContract]
    public interface IReportService
    {
        [OperationContract]
        IList<MailReport> GetMailReports(string from, DateTime start, DateTime end);
    }
}
