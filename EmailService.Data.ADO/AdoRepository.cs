﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using EmailService.Domain.Entity;
using EmailService.Domain.Enums;
using EmailService.Infrastructure.Dependency;
using EmailService.Infrastructure.Logger;
using EmailService.Infrastructure.Unity;

namespace EmailService.Data.ADO
{
    public class AdoRepository : IRepository
    {
        private readonly IServiceLocator _serviceLocator;
        private readonly ILogger _log;
        private readonly string _connectionString;

        public AdoRepository()
        {
            _serviceLocator = ServiceLocator.GetInstance();
            _log = _serviceLocator.GetService<ILogger>();
            _connectionString = ConfigurationManager.ConnectionStrings["DbConnection"].ConnectionString;
        }

        public IList<EmailMessage> GetMessagesToSend()
        {
            var messages = new List<EmailMessage>();

            using (var connection = OpenConnection())
            {
                if (connection == null)
                {
                    _log.Error("Can't get messages: connection failed");
                    return null;
                }
                const string proc = "GetMessagesToSend";
                _log.Debug("Getting messages from database");
                using (var command = new SqlCommand(proc, connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    try
                    {
                        var reader = command.ExecuteReader();
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                var message = new EmailMessage
                                {
                                    Id = reader.GetGuid(0),
                                    Message = reader.GetString(1),
                                    Subject = reader.GetString(2),
                                    SendDate = reader.GetDateTime(3),
                                    From = reader.GetString(4),
                                    Status = (MailStatus) reader.GetInt32(5),
                                    To = reader.GetString(6)
                                };
                                messages.Add(message);
                            }
                        }
                        reader.Close();
                    }
                    catch (Exception e)
                    {
                        _log.Error($"Can't get messages from DB cause of: {e}");
                        return null;
                    }
                }
            }
            return messages;
        }

        public IList<MailReport> GetReport(string from, DateTime start, DateTime end)
        {
            var reports = new List<MailReport>();

            using (var connection = OpenConnection())
            {
                if (connection == null)
                {
                    _log.Error("Can't get report: connection failed");
                    return null;
                }
                const string proc = "GetMessagesToReport";
                _log.Debug("Getting messages from database");
                using (var command = new SqlCommand(proc, connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add("@fromUser", SqlDbType.NVarChar).Value = from;
                    command.Parameters.Add("@startDate", SqlDbType.DateTime).Value = start;
                    command.Parameters.Add("@endDate", SqlDbType.DateTime).Value = end;
                    try
                    {
                        var reader = command.ExecuteReader();
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                var report = new MailReport
                                {
                                    Status = (MailStatus) reader.GetInt32(0),
                                    Subject = reader.GetString(1),
                                    SendDate = reader.GetDateTime(2)
                                };
                                reports.Add(report);
                            }
                        }
                        reader.Close();
                    }
                    catch (Exception e)
                    {
                        _log.Error($"Can't get reports from DB cause of: {e}");
                        return null;
                    }
                }
            }
            return reports;
        }

        public IResourceLocation ResourceLocation { get; set; }

        public void Update(EmailMessage message)
        {
            using (var connection = OpenConnection())
            {
                if (connection == null)
                {
                    _log.Error("Can't update message: connection failed");
                    return;
                }
                _log.Debug("Updating message status");
                const string proc = "UpdateMessageStatus";
                using (var command = new SqlCommand(proc, connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add("@id", SqlDbType.UniqueIdentifier).Value = message.Id;
                    command.Parameters.Add("@status", SqlDbType.Int).Value = message.Status;
                    try
                    {
                        var rows = command.ExecuteNonQuery();
                        _log.Debug($"Status updated: {rows} row(s) affected");
                        _log.Info($"New message status is: {message.Status}");
                    }
                    catch (Exception e)
                    {
                        _log.Error($"Can't update message cause of: {e}");
                    }
                } 
            }
        }
        public void Add(EmailMessage message)
        {
            using (var connection = OpenConnection())
            {
                if (connection == null)
                {
                    _log.Error("Can't update message: connection failed");
                    return;
                }
                _log.Debug("Adding message");
                const string proc = "AddMessage";
                using (var command = new SqlCommand(proc, connection))
                {
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.Add("@messageId", SqlDbType.UniqueIdentifier).Value = message.Id;
                    command.Parameters.Add("@statusId", SqlDbType.Int).Value = message.Status;
                    command.Parameters.Add("@text", SqlDbType.NVarChar).Value = message.Message;
                    command.Parameters.Add("@subject", SqlDbType.NVarChar).Value = message.Subject;
                    command.Parameters.Add("@sendDate", SqlDbType.DateTime).Value = message.SendDate;
                    command.Parameters.Add("@fromUser", SqlDbType.NVarChar).Value = message.From;
                    command.Parameters.Add("@toUser", SqlDbType.NVarChar).Value = message.To;
                    try
                    {
                        var rows = command.ExecuteNonQuery();
                        _log.Debug($"Message added: {rows} row(s) affected");
                        _log.Info("A new message has been added to DB");
                    }
                    catch (Exception e)
                    {
                        _log.Error($"Can't add message cause of: {e}");
                    }
                }
            }
        }

        private SqlConnection OpenConnection()
        {
            try
            {
                var connection = new SqlConnection(_connectionString);
                connection.Open();
                _log.Debug("Open database connection");
                return connection;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                _log.Error($"Can't open database connection: {e}");
                return null;
            }  
        }

    }
}
