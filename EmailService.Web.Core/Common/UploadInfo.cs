﻿using System.Collections.Generic;

namespace EmailService.Web.Core.Common
{
    public class UploadInfo
    {
        public List<int> ErrorLines { get; }
        public List<int> InvalidMessageLines { get; }
        public int UploadedMessagesCount { get; set; }

        public string ErrorLinesInfo
        {
            get
            {
                if (ErrorLines.Count == 0) return string.Empty;
                var info = "Error in lines: ";
                foreach (var line in ErrorLines) info += $"{line} ";
                return info;
            }
        }

        public string InvalidLinesInfo
        {
            get
            {
                if (InvalidMessageLines.Count == 0) return string.Empty;
                var info = "Invalid messages in lines: ";
                foreach (var line in ErrorLines) info += $"{line} ";
                return info;
            }
        }

        public UploadInfo()
        {
            ErrorLines = new List<int>();
            InvalidMessageLines = new List<int>();
        }

        public override string ToString()
        {
            var uploaded = $"Uploaded {UploadedMessagesCount} messages. ";
            if (ErrorLines.Count > 0) uploaded += $"Can't upload {ErrorLines.Count} messages. ";
            if (InvalidMessageLines.Count > 0) uploaded += $"{InvalidMessageLines.Count} messages is not valid.";
            return uploaded;
        }
    }
}
