﻿using System;
using System.IO;

namespace EmailService.Web.Core.Views
{
    public interface ISendMailView
    {
        string To { get; set; }
        string From { get; set; }
        string Subject { get; set; }
        string Message { get; set; }
        DateTime SendDate { get; set; }
        Stream UploadedFileStream { get; }
        //string FileName { get; }

        event EventHandler SendMessage;
        event EventHandler SendMessages;

        void ClearForm();
        void ClearInfo();
    }
}