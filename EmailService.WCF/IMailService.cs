﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using EmailService.Domain;
using EmailService.Domain.Entity;

namespace EmailService.WCF
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IMailService" in both code and config file together.
    [ServiceContract]
    public interface IMailService : ISender
    {
        [OperationContract]
        void SendMessage(EmailMessage message);

        [OperationContract]
        void SendMessages(ICollection<EmailMessage> messages);
    }
}
