﻿using System;
using System.IO;
using EmailService.Data;

namespace EmailService.WindowsService
{
   public class WinServiceResourceLocation : IResourceLocation
    {
        public string Location { get; set; }

        public WinServiceResourceLocation()
        {
            Location = Directory.GetParent(AppDomain.CurrentDomain.BaseDirectory).Parent.Parent.Parent.FullName;
        }
    }
}
