﻿using System;
using System.Threading;
using EmailService.Data;
using EmailService.Domain;
using EmailService.Infrastructure.Dependency;
using EmailService.Infrastructure.Logger;
using EmailService.Infrastructure.Unity;

namespace EmailService.ServiceConsole
{
    public class Program
    {
        private const int _interval = 90000; //15 sec
        private static Timer _timer;
        private static IRepository _mailRepository;
        private static ISender _mailSender;
        private static IServiceLocator _serviceLocator;
        private static ILogger _log;


        static void Main(string[] args)
        {
            _serviceLocator = ServiceLocator.GetInstance();

            _mailSender = _serviceLocator.GetService<ISender>();
            _mailRepository = _serviceLocator.GetService<IRepository>();
            _log = _serviceLocator.GetService<ILogger>();

            _log.Info("Starting service...");

            _timer = new Timer(OnWorking, null, 0, _interval);
            Console.ReadLine();
            _log.Info("Service stopped");
        }

        private static void OnWorking(object sender)
        {
            Console.WriteLine("Checking mail...");
            _log.Info("Checking mail...");
            var messages = _mailRepository.GetMessagesToSend();
            if (messages == null)
            {
                Console.WriteLine("Can't load messages");
                _log.Error("Can't load messages");
            }
            else
            {
                Console.WriteLine($"{messages.Count} messages found");
                _log.Info($"{messages.Count} messages found");
                _mailSender.SendMessages(messages);
            }
            Console.WriteLine("________________");
        }

    }
}
