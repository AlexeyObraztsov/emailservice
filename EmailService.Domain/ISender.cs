﻿using System.Collections.Generic;
using EmailService.Domain.Entity;

namespace EmailService.Domain
{
    public interface ISender
    {
        void SendMessage(EmailMessage message);
        void SendMessages(ICollection<EmailMessage> messages);
    }
}