﻿using System;
using System.Collections.Generic;
using EmailService.Data;
using EmailService.Domain.Entity;
using EmailService.Infrastructure.Dependency;
using EmailService.Infrastructure.Logger;
using EmailService.Infrastructure.Unity;

namespace EmailService.WCF.Report
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "ReportService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select ReportService.svc or ReportService.svc.cs at the Solution Explorer and start debugging.
    public class ReportService : IReportService
    {
        private readonly IRepository _mailRepository;
        private readonly IServiceLocator _serviceLocator;
        private readonly ILogger _log;

        public ReportService()
        {
            _serviceLocator = ServiceLocator.GetInstance();
            _log = _serviceLocator.GetService<ILogger>();
            _mailRepository = _serviceLocator.GetService<IRepository>();
        }

        public IList<MailReport> GetMailReports(string from, DateTime start, DateTime end)
        {
            var reports = _mailRepository.GetReport(from, start, end);
            if (reports == null)
            {
                _log.Error("Can't load reports");
                return null;
            }
            _log.Info("Getting mail report");
            _log.Debug($"{reports.Count} reports loaded");
            return reports;
        }
    }
}
