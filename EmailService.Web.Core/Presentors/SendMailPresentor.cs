﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using EmailService.Web.Core.Common;
using EmailService.Web.Core.MailService;
using EmailService.Web.Core.Views;
using EmailService.WebForms.Common;

namespace EmailService.Web.Core.Presentors
{
    public class SendMailPresentor : IPresentor
    {
        private readonly ISendMailView _view;
        private readonly List<EmailMessage> _messagesToSend;
        private readonly MessageValidation _validation;
        private readonly IMailService _mailService;

        private const int AdressMaxLenght = 50;
        private const int SubjectMaxLenght = 30;

        public UploadInfo UploadInfo { get; private set; }

        public SendMailPresentor(ISendMailView view) : this(view, new MailServiceClient()) { }

        public SendMailPresentor(ISendMailView view, IMailService mailService)
        {
            _view = view;
            _mailService = mailService;
            _messagesToSend = new List<EmailMessage>();
            _validation = new MessageValidation();

            _view.SendMessage += OnSendMessage;
            _view.SendMessages += OnSendMessages;
        }

        public void InitView(bool isPostBack)
        {
            _view.ClearInfo();
            if (!isPostBack)
            {
                _view.SendDate = DateTime.Now;
            }
        }

        public void OnSendMessage(object sender, EventArgs eventArgs)
        {
            var email = new EmailMessage
            {
                Id = Guid.NewGuid(),
                From = _view.From,
                To = _view.To,
                Subject = _view.Subject,
                Message = _view.Message,
                SendDate = _view.SendDate,
                Status = MailStatus.Pending
            };
            SendMessage(email);
            _view.ClearForm();
        }

        public void OnSendMessages(object sender, EventArgs eventArgs)
        {
            UploadCsv();
            SendMessages(_messagesToSend.ToArray());
        }

        public void SendMessage(EmailMessage message)
        {
            _mailService.SendMessage(message);
        }

        public void SendMessages(EmailMessage[] messages)
        {
            _mailService.SendMessages(messages);
        }

        public void UploadCsv()
        {
            var lineCounter = 0;
            const int headerLine = 1;
            UploadInfo = new UploadInfo();
            try
            {
                using (var reader = new StreamReader(_view.UploadedFileStream))
                {
                    while (true)
                    {
                        lineCounter++;
                        var line = reader.ReadLine();
                        if (line == null) break;
                        var parts = line.Split(',');
                        if (lineCounter == headerLine) continue;
                        var message = CreateMessage(parts);
                        if (message == null)
                        {
                            UploadInfo.ErrorLines.Add(lineCounter);
                            continue;
                        }
                        if (_validation.IsValidMessage(message, AdressMaxLenght, SubjectMaxLenght))
                        {
                            _messagesToSend.Add(message);
                            continue;
                        }
                        UploadInfo.InvalidMessageLines.Add(lineCounter);
                    }
                    UploadInfo.UploadedMessagesCount = _messagesToSend.Count;
                }
            }
            catch (Exception)
            {
                UploadInfo = null;
            }
        }

        public EmailMessage CreateMessage(string[] parts)
        {
            var to = parts[0];
            if (string.IsNullOrEmpty(to)) return null;
            var subject = parts[1];
            if (string.IsNullOrEmpty(subject)) return null;
            var message = parts[2];
            if (string.IsNullOrEmpty(message)) return null;

            DateTime sendDate;
            var isParsed = DateTime.TryParse(parts[3], out sendDate);
            if (!isParsed) return null;

            return new EmailMessage
            {
                Id = Guid.NewGuid(),
                From = _view.From,
                To = to,
                Subject = subject,
                Message = message,
                SendDate = sendDate,
                Status = MailStatus.Pending
            };
        }

        
    }
}
