﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using EmailService.Data.Xml.Extensions;
using EmailService.Domain.Entity;
using EmailService.Domain.Enums;
using EmailService.Infrastructure.Dependency;
using EmailService.Infrastructure.Logger;
using EmailService.Infrastructure.Unity;

namespace EmailService.Data.Xml
{
    public class XmlRepository : IRepository
    {
        private const string FileName = "shared.xml";
        private readonly ILogger _log;
        private readonly IServiceLocator _serviceLocator;

        public XmlRepository()
        {
            _serviceLocator = ServiceLocator.GetInstance();
            _log = _serviceLocator.GetService<ILogger>();
            _log.Debug("XmlRepository initialization");
        }

        public string Filepath => GetFilePath();

        private string GetFilePath()
        {
            var domainPath = ResourceLocation.Location;
            _log.Debug($"Resourece location: {ResourceLocation.Location}");
            return $@"{domainPath}\{FileName}";
        }

        private FileStream WaitForFile(string path, FileMode mode, FileAccess access, FileShare share)
        {
            int numTries;
            for (numTries = 0; numTries < 10; numTries++)
            {
                try
                {
                    var fs = new FileStream(path, mode, access, share);
                    _log.Debug($"File is ready after {numTries} tries");
                    return fs;
                }
                catch (IOException ex)
                {
                    _log.Warn($"Waiting for file {path} failed cause of: {ex.Message}");
                }
            }
            _log.Error($"Waiting for file {path} giving up after 10 tries");
            return null;
        }

        #region IRepository members

        public IList<EmailMessage> GetMessagesToSend()
        {
            var formatter = new XmlSerializer(typeof(List<EmailMessageDTO>));
            List<EmailMessage> messages;

            using (var fs = WaitForFile(Filepath, FileMode.OpenOrCreate, FileAccess.Read, FileShare.None))
            {
                if (fs == null)
                {
                    _log.Warn($"Can't get access to a file {Filepath}");
                    return null;
                }
                var dtoMessages = (List<EmailMessageDTO>) formatter.Deserialize(fs);
                messages = dtoMessages.ToDomainCollection();
                _log.Debug($"Found total {messages.Count} messages");
            }

            var messagesToSend = messages.Where(m =>(m.SendDate <= DateTime.Now) 
                                            && (m.Status != MailStatus.Sended)
                                            && (m.Status != MailStatus.Undelivered)).ToList();
            _log.Info($"Messages to send: {messagesToSend.Count}");

            return messagesToSend;
        }

        public void Add(EmailMessage message)
        {
            var formatter = new XmlSerializer(typeof(List<EmailMessageDTO>));
            var dtoMessages = new List<EmailMessageDTO>();

            using (var fs = WaitForFile(Filepath, FileMode.OpenOrCreate, FileAccess.Read, FileShare.None))
            {
                if (fs == null)
                {
                    _log.Warn($"Can't get access to a file {Filepath}");
                    return;
                }
                if (fs.Length != 0) dtoMessages = (List<EmailMessageDTO>) formatter.Deserialize(fs);
            }

            dtoMessages.Add(message.ToDto());

            using (var fs = WaitForFile(Filepath, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.None))
            {
                if (fs == null)
                {
                    _log.Warn($"Can't get access to a file {Filepath}");
                    return;
                }
                formatter.Serialize(fs, dtoMessages);
            }
            _log.Debug($"Serializing message {message.Id} to XML");
            _log.Info("Message added into Xml Repository");
        }

        public void Update(EmailMessage message)
        {
            var formatter = new XmlSerializer(typeof(List<EmailMessageDTO>));
            List<EmailMessageDTO> dtoMessages;

            using (var fs = WaitForFile(Filepath, FileMode.OpenOrCreate, FileAccess.Read, FileShare.None))
            {
                if (fs == null)
                {
                    _log.Warn($"Can't get access to a file {Filepath}");
                    return;
                }
                dtoMessages = (List<EmailMessageDTO>) formatter.Deserialize(fs);
            }
            var index = dtoMessages.FindIndex(m => m.Id == message.Id);
            dtoMessages[index] = message.ToDto();

            using (var fs = WaitForFile(Filepath, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.None))
            {
                _log.Debug($"Updating {message.Id} message in file");
                formatter.Serialize(fs, dtoMessages);
            }
            _log.Info("Message updated");
        }

        public IList<MailReport> GetReport(string from, DateTime start, DateTime end)
        {
            throw new NotImplementedException(); //TODO: Implement
        }

        public IResourceLocation ResourceLocation { get; set; }

        #endregion
    }
}