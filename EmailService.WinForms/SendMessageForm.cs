﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Forms;
using EmailService.Infrastructure.Dependency;
using EmailService.WinForms.Common;
using EmailService.Infrastructure.Logger;
using EmailService.Infrastructure.Unity;
using EmailService.WinForms.MailService;


namespace EmailService.WinForms
{
    public partial class SendMessageForm : Form
    {
        private readonly MailServiceClient _client;
        private readonly MessageValidation _validation;
        private List<EmailMessage> _messagesToSend;
        private readonly ILogger _log;
        private readonly IServiceLocator _serviceLocator;

        private const int AdressMaxLenght = 20;
        private const int SubjectMaxLenght = 20;
        private const string ClientAdress = "noreply@host.org";
        

        public SendMessageForm()
        {
            InitializeComponent();

            _serviceLocator = ServiceLocator.GetInstance();
            _log = _serviceLocator.GetService<ILogger>();

            timePickerSendAt.Value = DateTime.Now;
            _client = new MailServiceClient();
            _validation = new MessageValidation();

            btnSendMessages.Enabled = false;

            _log.Debug("Form initialization");
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            timePickerSendAt.Enabled = !checkBox1.Checked;
        }

        private void btnSend_Click(object sender, EventArgs e)
        {
            btnSend.Enabled = false;
            _log.Debug("Send button has been clicked");
            EmailMessage mailMessage;
            if (!ValidateMessage(out mailMessage))
            {
                lblValidationMessage.Text = @"Incorrect data";
                _log.Info("Message is not valid. Abort sending.");
                btnSend.Enabled = true;
                return;
            }
            _client.SendMessage(mailMessage);
            _log.Info("Sending message.");
            lblValidationMessage.ResetText();
            ClearForm();
            btnSend.Enabled = true;
        }

        public bool ValidateMessage(out EmailMessage message)
        {
            message = new EmailMessage
            {
                Id = Guid.NewGuid(),
                From = ClientAdress,
                To = txtBoxTo.Text,
                Subject = txtBoxSubject.Text,
                Message = rtBoxMessage.Text,
                SendDate = timePickerSendAt.Value,
                Status = MailStatus.Pending
            };
            return _validation.IsValidMessage(message, AdressMaxLenght, SubjectMaxLenght);
        }


        private void ClearForm()
        {
            txtBoxTo.ResetText();
            txtBoxSubject.ResetText();
            rtBoxMessage.ResetText();
            timePickerSendAt.Value = DateTime.Now;
        }


        private async void btnLoad_Click(object sender, EventArgs e)
        {
            var fileDialog = new OpenFileDialog
            {
                Title = @"Choose csv file with emails",
                Filter = @"CSV files|*.csv"
            };
            if (fileDialog.ShowDialog() != DialogResult.OK) return;
            _log.Debug("File has been opened.");
            var fileName = fileDialog.FileName;

            var uploadInfo = await Task.Run(() => LoadFromCsv(fileName));
            _log.Debug("Loading messages from file...");
            var infoString = $@"{uploadInfo} {Environment.NewLine} {uploadInfo.ErrorLinesInfo} {uploadInfo.InvalidLinesInfo}";
            MessageBox.Show(infoString);
            _log.Info(infoString);
            _messagesToSend = uploadInfo.UploadedMessages;
            if (_messagesToSend.Count == 0)
            {
                _log.Info("No messages to send");
                return;
            }
            btnSendMessages.Enabled = true;
            lblMessagesLoaded.Text = _messagesToSend.Count.ToString();
            _log.Info($"{_messagesToSend.Count} messages ready to send");
        }

        public UploadInfo LoadFromCsv(string path)
        {
            var lineCounter = 0;
            var uploadInfo = new UploadInfo();

            using (var reader = new StreamReader(path))
            {
                while (true)
                {
                    lineCounter++;
                    var line = reader.ReadLine();
                    if (line == null) break;
                    var parts = line.Split(',');
                    if (lineCounter == 1) continue;
                    var message = CreateMessage(parts);
                    if (message == null)
                    {
                        uploadInfo.ErrorLines.Add(lineCounter);
                        continue;
                    }
                    if (_validation.IsValidMessage(message, AdressMaxLenght, SubjectMaxLenght))
                    {
                        uploadInfo.UploadedMessages.Add(message);
                        continue;
                    }
                    uploadInfo.InvalidMessageLines.Add(lineCounter);
                } 
            }
            return uploadInfo;
        }

        public EmailMessage CreateMessage(string[] parts)
        {
            var to = parts[0];
            if (string.IsNullOrEmpty(to)) return null;
            var subject = parts[1];
            if (string.IsNullOrEmpty(subject)) return null;
            var message = parts[2];
            if (string.IsNullOrEmpty(message)) return null;
           

            DateTime sendDate;
            var isParsed = DateTime.TryParse(parts[3], out sendDate);
            if (!isParsed) return null;

            return new EmailMessage
            {
                Id = Guid.NewGuid(),
                From = ClientAdress,
                To = to,
                Subject = subject,
                Message = message,
                SendDate = sendDate,
                Status = MailStatus.Pending
            };

        }

        private async void btnSendMessages_Click(object sender, EventArgs e)
        {
            _log.Debug("Send Messages button has been clicked");
            await _client.SendMessagesAsync(_messagesToSend.ToArray());
            _log.Info("Sending messages..");
            btnSendMessages.Enabled = false;
            lblMessagesLoaded.Text = @"0";
        }

        private void btnViewReport_Click(object sender, EventArgs e)
        {
            var form = new MailReportsForm();
            form.Show();
        }
    }
}
