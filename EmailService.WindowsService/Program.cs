﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using EmailService.Infrastructure.Logger;
using EmailService.Infrastructure.Unity;

namespace EmailService.WindowsService
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            var serviceLocator = ServiceLocator.GetInstance();
            var log = serviceLocator.GetService<ILogger>();
            log.Debug("Main method enter");
            try
            {
                ServiceBase[] ServicesToRun;
                ServicesToRun = new ServiceBase[]
                {
                    new MailService()
                };
                log.Debug("ServicesToRun initialization");
                ServiceBase.Run(ServicesToRun);
                log.Debug("Run service");
            }
            catch (Exception e)
            {
                log.Fatal($"Service start failed: {e}");
            }
        }
    }
}
