﻿namespace EmailService.Infrastructure.Dependency
{
    public interface IServiceLocator
    {
        T GetService<T>();
    }
}