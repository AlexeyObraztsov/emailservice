﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/
USE EmailServiceDB
GO
-----

DELETE MailUsersMessages
GO
-----

DELETE Messages
GO
-----

DELETE MailUsers
GO
-----

DELETE MailStatus
GO
-----

INSERT INTO MailStatus (Id, Status) VALUES (1, 'Pending')
INSERT INTO MailStatus (Id, Status) VALUES (2, 'Sended')
INSERT INTO MailStatus (Id, Status) VALUES (3, 'Undelivered')
INSERT INTO MailStatus (Id, Status) VALUES (4, 'MailboxUnavailible')
-----

INSERT INTO MailUsers (Id, Adress) VALUES ('AA303191-9092-4450-AF63-BF9465DE357F', 'noreply@host.org')
INSERT INTO MailUsers (Id, Adress) VALUES ('C13090E5-CF51-44FC-A81D-774DE6281763', 'hello@gmail.com')
INSERT INTO MailUsers (Id, Adress) VALUES ('C46066A6-C659-4B5E-B5DF-7DD538E10C9F', 'hello2@gmail.com')
INSERT INTO MailUsers (Id, Adress) VALUES ('C989151F-B622-438F-B876-91D106B25DBB', 'hello3@gmail.com')
-----

INSERT INTO Messages (Id, StatusId, Text, Subject, SendDate, FromUser) VALUES ('B553F4E7-C002-432F-B605-5555B8A26368', 1, 'hi', 'hello', '2016-08-02 11:23', 'AA303191-9092-4450-AF63-BF9465DE357F')
INSERT INTO Messages (Id, StatusId, Text, Subject, SendDate, FromUser) VALUES ('22E4EE0E-B1A4-4B0C-AF61-57D942D6478E', 1, 'hi', 'hello2', '2016-08-02 09:11', 'AA303191-9092-4450-AF63-BF9465DE357F') 
INSERT INTO Messages (Id, StatusId, Text, Subject, SendDate, FromUser) VALUES ('21671EFC-4AFC-49C2-AB4E-9362FFC04BE1', 1, 'hi', 'hello3', '2016-08-02 14:48', 'AA303191-9092-4450-AF63-BF9465DE357F')
-----

INSERT INTO MailUsersMessages (MessageId, MailUserId) VALUES('B553F4E7-C002-432F-B605-5555B8A26368', 'C13090E5-CF51-44FC-A81D-774DE6281763')
INSERT INTO MailUsersMessages (MessageId, MailUserId) VALUES('22E4EE0E-B1A4-4B0C-AF61-57D942D6478E', 'C46066A6-C659-4B5E-B5DF-7DD538E10C9F')
INSERT INTO MailUsersMessages (MessageId, MailUserId) VALUES('21671EFC-4AFC-49C2-AB4E-9362FFC04BE1', 'C989151F-B622-438F-B876-91D106B25DBB')
-----