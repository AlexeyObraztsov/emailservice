﻿<%@ Page Title="EmailService" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="EmailService.WebForms._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="row" style="
    padding-left: 15%;
    padding-right: 18%;
">
        <div id="panel-send-message" class="panel panel-info">
            <div class="panel panel-heading">Send message</div>
            <div class="pamel-body">
                <div class="form-horizontal">
                    <div class="form-group">
                        <label class="control-label col-md-2">To:</label>
                        <div class="col-md-10"><input class="form-control" type="email" runat="server" ID="TextBoxTo" required/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-2">Subject:</label>
                        <div class="col-md-10"><input class="form-control" type="text" runat="server" ID="TextBoxSubject" required maxlength="20"/>
                        </div>
                    </div>
                    <asp:PlaceHolder ID="PlaceHolder1" runat="server"></asp:PlaceHolder>
                    <div class="form-group">
                        <label class="control-label col-md-2">Message:</label>
                        <div class="col-md-10"><textarea class="form-control message-area" type="text" runat="server" ID="TextBoxMessage" required></textarea>
                        </div>
                    </div>
                      <div class="form-group">
                          <label class="control-label col-md-2">Send at:</label>
                          <div class="col-md-4">
                              <input type="datetime-local" class="form-control" runat="server" ID="TimePickerSendDate" required disabled/>
                          </div> 
                        <div class="col-sm-1">
                            <div class="checkbox">
                                <label><input type="checkbox" runat="server" ID="CheckboxNow" checked onchange="handleChange(this)"/>Now</label>    
                            </div>
                         </div>
                     </div>
                    <div class="form-group">
                        <div class="col-md-offset-2 col-md-2">
                            <input type="submit" class="btn btn-default" value="Send" runat="server" OnServerClick="BtnSend_OnServerClick"/>
                        </div>
                        <div class="col-md-2 col-md-pull-1 message-label">
                            <span class="text-success" runat="server" ID="LblSuccess"></span>
                        </div>
                    </div>
                    <hr/>
                    <div class="form-group">
                        <label class="control-label col-md-2">Load from file:</label>
                        <div class="col-md-6">
                            <input type="file" title="Choose file to upload" class="btn btn-primary" runat="server" ID="UploadedFile"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-2 col-md-offset-2">
                            <input type="button" class="btn btn-default" runat="server" value="Send Messages" ID="BtnSendMessages" OnServerClick="BtnSendMessages_OnServerClick"/>
                        </div>
                        <div class="col-md-3">
                            <label class="message-label text-info">Messages Sended: <span runat="server" ID="LblLoaded"></span></label>
                        </div>
                        <div class="col-md-5 message-label ">
                            <span class="text-danger" runat="server" ID="LblLoadError"></span>
                        </div>
                    </div>
                         
                    
                </div>
            </div>
        </div>
    </div>

<script>
    function handleChange(ch) {
        var el = document.getElementById('MainContent_TimePickerSendDate');
        if (ch.checked == true) {
            el.disabled = true;
        } else {
            el.disabled = false;
        }
    }

    function addClass(el, className) {
        if (el.classList)
            el.classList.add(className);
        else
            el.className += ' ' + className;
    }

    document.addEventListener("DOMContentLoaded", function () {
        $('input[type=file]').bootstrapFileInput();

        var sendBtn = document.getElementById('MainContent_BtnSendMessages');
        sendBtn.disabled = true;
        var input = document.getElementById('MainContent_UploadedFile');
        input.addEventListener("change",
            function() {
                sendBtn.disabled = false;
            });
    });
</script>
<script src="/Scripts/bootstrap.file-input.js"></script>
</asp:Content>

