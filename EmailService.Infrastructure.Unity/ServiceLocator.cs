﻿using System;
using EmailService.Infrastructure.Dependency;
using Microsoft.Practices.Unity;

namespace EmailService.Infrastructure.Unity
{
    public class ServiceLocator : IServiceLocator
    {
        private readonly IUnityContainer _container;
        private static IServiceLocator _instance;
        private static readonly object Locker = new object();

        private ServiceLocator()
        {
            _container = UnityComponentConfiguration.ConfigureContainer();
        }

        public static IServiceLocator GetInstance()
        {
            lock (Locker)
            {
                return _instance ?? (_instance = new ServiceLocator());
            }
        }

        public T GetService<T>()
        {
            return _container.Resolve<T>();
        }
    }
}
