﻿using System;
using EmailService.Domain.Enums;

namespace EmailService.Domain.Entity
{
    public class EmailMessage
    {
        public Guid Id { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }
        public DateTime SendDate { get; set; }
        public MailStatus Status { get; set; }
    }
}