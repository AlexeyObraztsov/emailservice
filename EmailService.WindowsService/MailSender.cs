﻿using System;
using System.Collections.Generic;
using System.Net.Mail;
using EmailService.Data;
using EmailService.Domain;
using EmailService.Domain.Entity;
using EmailService.Domain.Enums;
using EmailService.Infrastructure.Dependency;
using EmailService.Infrastructure.Logger;
using EmailService.Infrastructure.Unity;

namespace EmailService.WindowsService
{
    public class MailSender : ISender
    {
        private const int Port = 25;
        private const string Host = "mail.o2online.de"; //TODO: Create config object

        private readonly SmtpClient _client;
        private readonly IRepository _mailRepository;
        private readonly IServiceLocator _serviceLocator;
        private readonly ILogger _log;

        public MailSender()
        {
            _client = new SmtpClient
            {
                Host = Host,
                Port = Port,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = true
            };
            _serviceLocator = ServiceLocator.GetInstance();
            _mailRepository = _serviceLocator.GetService<IRepository>();
            _log = _serviceLocator.GetService<ILogger>();
        }

        #region ISender members

        public void SendMessage(EmailMessage message)
        {
            var messageToSend = GetMessageToSend(message);
            try
            {
                _client.Send(messageToSend);
                message.Status = MailStatus.Sended;
                _mailRepository.Update(message);
                Console.WriteLine("messageSended");
                _log.Info("Message sended");
            }
            catch (SmtpException e)
            {
                Console.WriteLine(e.Message);
                _log.Warn(e.Message);
                if (e.StatusCode == SmtpStatusCode.MailboxUnavailable)
                {
                    _log.Warn("Message wasn't sent, mailbox is unavailible");
                    if (message.Status == MailStatus.MailboxUnavailible) return;
                    message.Status = MailStatus.MailboxUnavailible;
                    _mailRepository.Update(message);
                    _log.Debug("Updating message status");
                }
                else
                {
                    _log.Error($"Can't send message. SMTP status: {e.StatusCode}");
                    message.Status = MailStatus.Undelivered;
                    _mailRepository.Update(message);
                    _log.Debug("Updating message status");
                }
            }
        }

        public void SendMessages(ICollection<EmailMessage> messages)
        {
            _log.Debug($"Sending {messages.Count} messages");
            foreach (var message in messages)
            {
                SendMessage(message);
            }
        }

        #endregion


        private MailMessage GetMessageToSend(EmailMessage message)
        {
            var messageToSend = new MailMessage(message.From, message.To);
            messageToSend.Subject = message.Subject;
            messageToSend.Body = message.Message;
            return messageToSend;
        }

    }
}
