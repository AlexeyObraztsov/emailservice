﻿using System.Collections.Generic;
using EmailService.Data;
using EmailService.Domain.Entity;
using EmailService.Infrastructure.Dependency;
using EmailService.Infrastructure.Logger;
using EmailService.Infrastructure.Unity;

namespace EmailService.WCF
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "MailService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select MailService.svc or MailService.svc.cs at the Solution Explorer and start debugging.
    public class MailService : IMailService
    {
        private readonly IRepository _mailRepository;
        private readonly IServiceLocator _serviceLocator;
        private readonly ILogger _log;

        public MailService()
        {
            _serviceLocator = ServiceLocator.GetInstance();
            _log = _serviceLocator.GetService<ILogger>();
            _mailRepository = _serviceLocator.GetService<IRepository>();

            _log.Debug("MailRepository resolved");
        }

        public void SendMessage(EmailMessage message)
        {
            _mailRepository.Add(message);
            _log.Info("Adding message to repository");
        }

        public void SendMessages(ICollection<EmailMessage> messages)
        {
            foreach (var message in messages)
            {
                SendMessage(message);
            }
            _log.Info($"{messages.Count} messages added into repository");
        }
    }
}
