﻿using System.Text.RegularExpressions;
using EmailService.WinForms.MailService;


namespace EmailService.WinForms.Common
{
    public class MessageValidation
    {
        public bool IsValidLenght(string text, int lenght)
        {
            if (string.IsNullOrEmpty(text)) return false;
            return text.Length <= lenght;
        }

        public bool IsValidAdress(string text)
        {
            var regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
            return regex.Match(text).Success;
        }

        public bool IsValidMessage(EmailMessage message, int adressMaxLenght, int subjectMaxLenght)
        {
            var isValidLenght = IsValidLenght(message.To, adressMaxLenght) &&
                                IsValidLenght(message.Subject, subjectMaxLenght);
            if (!isValidLenght) return false;
            return IsValidAdress(message.To);
        }
    }
}
