﻿using System.ComponentModel;
using System.ServiceProcess;

namespace EmailService.WindowsService
{
    [RunInstaller(true)]
    public partial class MailServiceInstaller : System.Configuration.Install.Installer
    {
        ServiceInstaller serviceInstaller;
        ServiceProcessInstaller processInstaller;

        public MailServiceInstaller()
        {
            InitializeComponent();
            serviceInstaller = new ServiceInstaller();
            processInstaller = new ServiceProcessInstaller();

            processInstaller.Account = ServiceAccount.LocalSystem;
            serviceInstaller.StartType = ServiceStartMode.Automatic;
            serviceInstaller.ServiceName = "MailService";
            Installers.Add(processInstaller);
            Installers.Add(serviceInstaller);
        }
    }
}
