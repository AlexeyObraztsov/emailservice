﻿using EmailService.Infrastructure.Logger;
using log4net;
using log4net.Config;

namespace EmailService.Infrastructure.log4net
{
    public class Logger : ILogger
    {
        public static ILog Log { get; } = LogManager.GetLogger(typeof(Logger));

        public Logger()
        {
            XmlConfigurator.Configure();
        }

        #region ILogger members

        public void Debug(string message)
        {
            if (message != null)
                Log.Debug(message);
        }

        public void Info(string message)
        {
            if (message != null)
                Log.Info(message);
        }

        public void Warn(string message)
        {
            if (message != null)
                Log.Warn(message);
        }

        public void Error(string message)
        {
            if (message != null)
                Log.Error(message);
        }

        public void Fatal(string message)
        {
            if (message != null)
                Log.Fatal(message);
        }

        #endregion

    }
}
