﻿using System;
using System.Collections.Generic;
using EmailService.Domain.Entity;

namespace EmailService.Data
{
    public interface IRepository
    {
        IList<EmailMessage> GetMessagesToSend();
        void Add(EmailMessage message);
        void Update(EmailMessage message);
        IList<MailReport> GetReport(string from, DateTime start, DateTime end); 

        IResourceLocation ResourceLocation { get; set; }
    }
}