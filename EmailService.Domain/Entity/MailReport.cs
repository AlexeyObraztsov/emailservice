﻿using System;
using EmailService.Domain.Enums;

namespace EmailService.Domain.Entity
{
    public class MailReport
    {  
        public MailStatus Status { get; set; }
        public string Subject { get; set; }
        public DateTime SendDate { get; set; }
    }
}
