﻿using System;
using System.Xml.Serialization;
using EmailService.Domain.Enums;

namespace EmailService.Data.Xml
{
    [Serializable]
    [XmlRoot(ElementName = "EmailMessage")]
    public class EmailMessageDTO
    {
        public Guid Id { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }
        public MailStatus Status { get; set; }

        [XmlElement("SendDate")]
        public string SendDateString
        {
            get { return SendDate.ToString("g"); }
            set { SendDate = DateTime.Parse(value); }
        }

        [XmlIgnore]
        public DateTime SendDate { get; set; }
    }
}
