﻿using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Configuration;

namespace EmailService.Infrastructure.Unity
{
    public class UnityComponentConfiguration
    {
        public static IUnityContainer ConfigureContainer()
        {
            IUnityContainer container = new UnityContainer();
            return container.LoadConfiguration();
        }
    }
}
