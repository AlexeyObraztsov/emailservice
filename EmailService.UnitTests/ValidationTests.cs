﻿using EmailService.WinForms.Common;
using NUnit.Framework;

namespace EmailService.UnitTests
{
   [TestFixture]
    public class ValidationTests
    {
      public MessageValidation Validation { get; set; }

        public ValidationTests()
        {
            Validation = new MessageValidation();
        }
        
        [Test]
        public void TextLenghtGreaterThan20_ReturnsFalse()
        {
            var textMaxLenght = 20;
            var text = "thisTextIsGreaterThanTwentySymbols";
            var isValid = Validation.IsValidLenght(text, textMaxLenght);
            Assert.False(isValid);
        }

        [Test]
        public void TextLenghtLessThan20_ReturnsTrue()
        {
            var textMaxLenght = 20;
            var text = "thisTxtIsLess20";
            var isValid = Validation.IsValidLenght(text, textMaxLenght);
            Assert.True(isValid);
        }

        [Test]
        public void TextLenghtEquals20_ReturnsTrue()
        {
            var textMaxLenght = 20;
            var text = "thisTextIsEquals20!!";
            var i = text.Length;
            var isValid = Validation.IsValidLenght(text, textMaxLenght);
            Assert.True(isValid);
        }

        [Test]
        public void CorrectEmail_ReturnsTrue()
        {
            var text = "support@gmail.com";
            var isValidEmail = Validation.IsValidAdress(text);
            Assert.True(isValidEmail);
        }

        [Test]
        public void AdressWithoutAt_ReturnsFalse()
        {
            var text = "supportgmail.com";
            Assert.False(Validation.IsValidAdress(text));
        }

        [Test]
        public void AdressWithoutDot_ReturnsFalse()
        {
            var text = "support@gmailcom";
            Assert.False(Validation.IsValidAdress(text));
        }

        [Test]
        public void AdressWithoutName_ReturnsFalse()
        {
            var text = "@gmail.com";
            Assert.False(Validation.IsValidAdress(text));
        }


    }
}
