﻿using System;
using System.IO;
using EmailService.Data;

namespace EmailService.WCF
{
    public class WcfXmlResourceLocation : IResourceLocation
    {
        public string Location { get; set; }

        public WcfXmlResourceLocation()
        {
            Location = Path.GetDirectoryName(Path.GetDirectoryName(AppDomain.CurrentDomain.BaseDirectory));
        }
    }
}