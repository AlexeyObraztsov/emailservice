﻿namespace EmailService.Data
{
    public interface IResourceLocation
    {
        string Location { get; set; }
    }
}