﻿using System.IO;
using EmailService.Data;

namespace EmailService.ServiceConsole
{
    public class XmlRecourceLocation : IResourceLocation
    {
        public string Location { get; set; }

        public XmlRecourceLocation()
        {
            Location = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent.FullName;
        }
    }
}
