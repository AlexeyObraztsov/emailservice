﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using EmailService.Data;
using EmailService.Domain;
using EmailService.Infrastructure.Dependency;
using EmailService.Infrastructure.Logger;
using EmailService.Infrastructure.Unity;


namespace EmailService.WindowsService
{
    public partial class MailService : ServiceBase
    {
        private const int _interval = 15000; //15 sec
        private Timer _timer;
        private readonly IRepository _mailRepository;
        private readonly ISender _mailSender;
        private IServiceLocator _serviceLocator;
        private ILogger _log;

        public MailService()
        {

            _serviceLocator = ServiceLocator.GetInstance();
            _log = _serviceLocator.GetService<ILogger>();
            _log.Debug("Resolving dependencies");
            _mailSender = _serviceLocator.GetService<ISender>();
            _mailRepository = _serviceLocator.GetService<IRepository>();

            _log.Debug("Service initialization");

            InitializeComponent();

            this.CanStop = true;
            this.CanPauseAndContinue = true;
            this.AutoLog = true;
            _log.Debug("Service initialized");
        }

        protected override void OnStart(string[] args)
        {
            _log.Info("Starting service...");
            _timer = new Timer(OnWorking, null, 0, _interval);
        }

        private void OnWorking(object state)
        {
            _log.Info("Checking mail...");
            var messages = _mailRepository.GetMessagesToSend();
            if (messages == null) _log.Error("Can't load messages");
            else
            {
                _log.Info($"{messages.Count} messages found");
                _mailSender.SendMessages(messages);
            }
        }

        protected override void OnPause()
        {
            _log.Info("Service paused");
            Thread.Sleep(7000);
        }

        protected override void OnStop()
        {
            _log.Info("Service stopped");
            Thread.Sleep(7000);
        }
    }
}
